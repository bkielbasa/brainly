# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

This project was made for Brainly

### How do I get set up? ###

To run this project you need:

* php 7.1 installed
* xdebug installed (tests only)
* composer installed

To start working with the project you need to clone the repo and install dependencies:

```bash
git clone git@bitbucket.org:bkielbasa/brainly.git
cd brainly
composer install
```

To run unit and functional tests just call command

```bash
./vendor/bin/phpunit
```

On "production" you need to run database migrations to have schema up-to-date:

```bash
./bin/console doctrine:database:create
./bin/console doctrine:migrations:migrate
```

To run the project you can use built-in PHP web server.

```bash
php -S localhost:8000 -t public/
```