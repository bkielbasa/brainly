<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171217155039 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $question = $schema->createTable('question');

        $question->addColumn('id', 'bigint', [
            'autoincrement' => true,
        ]);
        $question->addColumn('content', 'string');
        $question->addColumn('created_at', 'integer');
        $question->setPrimaryKey(['id']);

        $answer = $schema->createTable('question_answer');
        $answer->addColumn('id', 'bigint', [
            'autoincrement' => true,
        ]);
        $answer->addColumn('content', 'string');
        $answer->addColumn('created_at', 'integer');
        $answer->addColumn('question_id', 'integer');
        $answer->setPrimaryKey(['id']);
    }

    public function down(Schema $schema)
    {
        $schema->dropTable('question');
        $schema->dropTable('question_answer');
    }
}
