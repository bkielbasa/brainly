<?php

namespace Brainly\Domain\Question;

class CreatedAt
{
    /**
     * @var \DateTimeImmutable
     */
    private $date;

    /**
     * CreatedAt constructor.
     * @param \DateTimeImmutable $date
     */
    public function __construct(\DateTimeImmutable $date)
    {
        $this->date = $date;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getValue(): \DateTimeImmutable
    {
        return $this->date;
    }
}
