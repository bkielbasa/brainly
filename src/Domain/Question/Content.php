<?php

namespace Brainly\Domain\Question;

class Content
{
    /**
     * @var string
     */
    private $value;

    /**
     * Content constructor.
     * @param string $value
     */
    public function __construct(string $value)
    {
        if (strlen($value) < 20) {
            throw new \InvalidArgumentException('The content cannot be shorten than 20');
        }

        if (strlen($value) > 5000) {
            throw new \InvalidArgumentException('The content cannot be longer than 5000');
        }

        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
}