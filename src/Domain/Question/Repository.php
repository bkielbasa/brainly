<?php

namespace Brainly\Domain\Question;

use Brainly\Domain\Question;

interface Repository
{
    /**
     * @param Id $id
     * @return Question
     */
    public function get(Id $id): Question;

    /**
     * @param string $content
     * @return Question
     */
    public function add(string $content): Question;

    /**
     * @return array
     */
    public function getAll(): array;

    /**
     * @param int $questionId
     * @param string $content
     * @return Question
     */
    public function updateQuestion(int $questionId, string $content): Question;

    /**
     * @param int $questionId
     */
    public function delete(int $questionId): void;
}
