<?php

namespace Brainly\Domain;

use Brainly\Domain\Question\Content;
use Brainly\Domain\Question\Id;

class Question
{
    /**
     * @var Id
     */
    private $id;

    /**
     * @var Question\Content
     */
    private $content;

    /**
     * @var Question\CreatedAt
     */
    private $createdAt;

    /**
     * Question constructor.
     * @param Id $id
     * @param Question\Content $content
     * @param Question\CreatedAt $createdAt
     */
    public function __construct(Id $id, Question\Content $content, Question\CreatedAt $createdAt = null)
    {
        $this->id = $id;
        $this->content = $content;

        if (is_null($createdAt)) {
            $createdAt = new Question\CreatedAt(new \DateTimeImmutable());
        }

        $this->createdAt = $createdAt;
    }

    /**
     * @return Id
     */
    public function getId(): Id
    {
        return $this->id;
    }

    /**
     * @return Question\Content
     */
    public function getContent(): Question\Content
    {
        return $this->content;
    }

    /**
     * @return Question\CreatedAt
     */
    public function getCreatedAt(): Question\CreatedAt
    {
        return $this->createdAt;
    }

    public function updateContent(string $content)
    {
        $this->content = new Content($content);
    }
}
