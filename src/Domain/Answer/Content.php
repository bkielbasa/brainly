<?php

namespace Brainly\Domain\Answer;

class Content
{
    /**
     * @var string
     */
    private $value;

    /**
     * Content constructor.
     * @param string $value
     */
    public function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
}