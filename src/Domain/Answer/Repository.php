<?php

namespace Brainly\Domain\Answer;

use Brainly\Domain\Answer;

interface Repository
{
    /**
     * @param Id $id
     * @return Answer
     */
    public function get(Id $id): Answer;

    /**
     * @param Id $id
     */
    public function remove(Id $id);

    /**
     * @param int $questionId
     * @return Answer[]
     */
    public function getAnswers(int $questionId): array;

    /**
     * @param int $questionId
     * @param string $content
     * @return Answer
     */
    public function add(int $questionId, string $content): Answer;

    /**
     * @param Id $id
     * @param Content $content
     * @return mixed
     */
    public function update(Id $id, Content $content): Answer;
}
