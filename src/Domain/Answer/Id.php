<?php

namespace Brainly\Domain\Answer;

class Id
{
    /**
     * @var int
     */
    private $id;

    /**
     * Id constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        if ($id < 1) {
            throw new \InvalidArgumentException('ID must be greaten than 0');
        }
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->id;
    }
}