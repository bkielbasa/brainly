<?php

namespace Brainly\Domain;

use Brainly\Domain\Answer\Content;
use Brainly\Domain\Answer\CreatedAt;
use Brainly\Domain\Answer\Id;

class Answer
{
    /**
     * @var  Id
     */
    private $id;

    /**
     * @var Answer\Content
     */
    private $content;

    /**
     * @var int
     */
    private $questionId;

    /**
     * @var CreatedAt
     */
    private $createdAt;

    /**
     * Answer constructor.
     * @param Id $id
     * @param Answer\Content $content
     * @param int $questionId
     * @param CreatedAt $createdAt
     */
    public function __construct(Id $id, Answer\Content $content, int $questionId, CreatedAt $createdAt = null)
    {
        $this->id = $id;
        $this->content = $content;
        $this->questionId = $questionId;

        if (is_null($createdAt)) {
            $createdAt = new CreatedAt(new \DateTimeImmutable());
        }

        $this->createdAt = $createdAt;
    }

    /**
     * @return Id
     */
    public function getId(): Id
    {
        return $this->id;
    }

    /**
     * @return Answer\Content
     */
    public function getContent(): Answer\Content
    {
        return $this->content;
    }

    /**
     * @return int
     */
    public function getQuestionId(): int
    {
        return $this->questionId;
    }

    /**
     * @return CreatedAt
     */
    public function getCreatedAt(): CreatedAt
    {
        return $this->createdAt;
    }

    public function setContent(Content $content)
    {
        $this->content = $content;
    }
}
