<?php

namespace Brainly\Infrastructure\Answer\Repository;

use Brainly\Domain\Answer;
use Brainly\Domain\Answer\Content;
use Brainly\Domain\Answer\Id;
use Brainly\Domain\Answer\Repository;

class InMemoryRepository implements Repository
{
    /**
     * @var Answer[]
     */
    private $answers = [];

    /**
     * InMemoryRepository constructor.
     * @param Answer[] $answers
     */
    public function __construct(array $answers = [])
    {
        $this->answers = $answers;
    }

    /**
     * @param Id $id
     * @return Answer
     */
    public function get(Id $id): Answer
    {
        if (!isset($this->answers[$id->getValue()])) {
            throw new \InvalidArgumentException('Cannot find answer with the ID');
        }

        return $this->answers[$id->getValue()];
    }

    /**
     * @param int $questionId
     * @param string $content
     * @return Answer
     */
    public function add(int $questionId, string $content): Answer
    {
        $answer = new Answer(new Id(rand(1, 1000)), new Answer\Content($content), $questionId, new Answer\CreatedAt(new \DateTimeImmutable()));
        $this->answers[$answer->getId()->getValue()] = $answer;

        return $answer;
    }

    /**
     * @param int $questionId
     * @return Answer[]
     */
    public function getAnswers(int $questionId): array
    {
        $answers = [];

        foreach ($this->answers as $answer) {
            if ($answer->getQuestionId() == $questionId) {
                $answers[] = $answer;
            }
        }

        return $answers;
    }

    /**
     * @param Id $id
     */
    public function remove(Id $id)
    {
        unset($this->answers[$id->getValue()]);
    }

    /**
     * @param Id $id
     * @param Content $content
     * @return Answer
     */
    public function update(Id $id, Content $content): Answer
    {
        $answer = $this->get($id);
        $answer->setContent($content);
        $this->answers[$answer->getId()->getValue()] = $answer;

        return $answer;
    }
}
