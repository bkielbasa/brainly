<?php

namespace Brainly\Infrastructure\Answer\Repository;

use Brainly\Domain\Answer;
use Brainly\Domain\Answer\Content;
use Brainly\Domain\Answer\Id;
use Brainly\Domain\Answer\Repository;
use Doctrine\ORM\EntityManagerInterface;

class DoctrineRepository implements Repository
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * DoctrineRepository constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Id $id
     * @return Answer
     */
    public function get(Id $id): Answer
    {
        $entity = $this->em->getRepository(\Brainly\Entity\Answer::class)
            ->find($id->getValue());

        return $this->fromEntity($entity);
    }

    /**
     * @param int $questionId
     * @return Answer[]
     */
    public function getAnswers(int $questionId): array
    {
        $entityAnswers = $this->em->getRepository(\Brainly\Entity\Answer::class)->findBy([
            'question' => $questionId,
        ]);

        $answers = [];

        foreach ($entityAnswers as $entity) {
            $answers[] = $this->fromEntity($entity);
        }

        return $answers;
    }

    /**
     * @param int $questionId
     * @param string $content
     * @return Answer
     */
    public function add(int $questionId, string $content): Answer
    {
        $questionEntity = $this->em->getRepository(\Brainly\Entity\Question::class)
            ->find($questionId);

        $entity = new \Brainly\Entity\Answer();
        $entity->setContent($content);
        $entity->setCreatedAt(time());
        $entity->setQuestion($questionEntity);
        $this->em->persist($entity);
        $this->em->flush();

        return $this->fromEntity($entity);
    }

    /**
     * @param Id $id
     */
    public function remove(Id $id)
    {
        $entity = $this->em->getRepository(\Brainly\Entity\Answer::class)
            ->find($id->getValue());

        $this->em->remove($entity);
        $this->em->flush();
    }

    /**
     * @param Id $id
     * @param Content $content
     * @return Answer
     */
    public function update(Id $id, Content $content): Answer
    {
        $entity = $this->em->getRepository(\Brainly\Entity\Answer::class)
            ->find($id->getValue());
        $entity->setContent($content->getValue());

        $this->em->persist($entity);
        $this->em->flush();

        return $this->fromEntity($entity);
    }

    private function fromEntity(\Brainly\Entity\Answer $entity): Answer
    {
        $date = new \DateTimeImmutable();
        $date->setTimestamp($entity->getCreatedAt());

        return new Answer(new ID($entity->getId()), new Answer\Content($entity->getContent()), $entity->getQuestion()->getId(), new Answer\CreatedAt($date));
    }
}