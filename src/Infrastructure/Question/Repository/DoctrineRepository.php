<?php

namespace Brainly\Infrastructure\Question\Repository;

use Brainly\Domain\Question;
use Brainly\Domain\Question\Id;
use Brainly\Domain\Question\Repository;
use Doctrine\ORM\EntityManagerInterface;

class DoctrineRepository implements Repository
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * DoctrineRepository constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Id $id
     * @return Question
     */
    public function get(Id $id): Question
    {
        $entity = $this->em->getRepository(\Brainly\Entity\Question::class)
            ->find($id->getValue());

        if (empty($entity)) {
            throw new \InvalidArgumentException('Cannot find question with the ID');
        }

        return $this->fromEntity($entity);
    }

    /**
     * @param int $questionId
     * @return Question[]
     */
    public function getAnswers(int $questionId)
    {
        $entityAnswers = $this->em->getRepository(\Brainly\Entity\Question::class)->findBy([
            'question.id' => $questionId,
        ]);

        if (empty($entity)) {
            throw new \InvalidArgumentException('Cannot find question with the ID');
        }

        $answers = [];

        foreach ($entityAnswers as $entity) {
            $answers[] = $this->fromEntity($entity);
        }

        return $answers;
    }

    /**
     * @param string $content
     * @return Question
     */
    public function add(string $content): Question
    {
        $entity = new \Brainly\Entity\Question();
        $entity->setContent($content);
        $entity->setCreatedAT(time());
        $this->em->persist($entity);
        $this->em->flush();

        return $this->fromEntity($entity);
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        $entityAnswers = $this->em->getRepository(\Brainly\Entity\Question::class)->findAll();

        $answers = [];

        foreach ($entityAnswers as $entity) {
            $answers[] = $this->fromEntity($entity);
        }

        return $answers;
    }

    /**
     * @param int $questionId
     * @param string $content
     * @return Question
     */
    public function updateQuestion(int $questionId, string $content): Question
    {
        $entity =$this->em->getRepository(\Brainly\Entity\Question::class)->find($questionId);
        $question = new Question(new ID($entity->getId()), new Question\Content($entity->getContent()), $entity->getCreatedAt());
        $question->updateContent($content);
        $entity->setContent($question->getContent()->getValue());
        $this->em->persist($entity);
        $this->em->flush();

        return $question;
    }

    /**
     * @param int $questionId
     */
    public function delete(int $questionId): void
    {
        $entity =$this->em->getRepository(\Brainly\Entity\Question::class)->find($questionId);
        $this->em->remove($entity);
        $this->em->flush();
    }

    private function fromEntity(\Brainly\Entity\Question $entity): Question
    {
        $date = new \DateTimeImmutable();
        $date->setTimestamp($entity->getCreatedAt());

        return new Question(new ID($entity->getId()), new Question\Content($entity->getContent()), new Question\CreatedAt($date));
    }
}