<?php

namespace Brainly\Infrastructure\Question\Repository;

use Brainly\Domain\Question;
use Brainly\Domain\Question\Id;
use Brainly\Domain\Question\Repository;

class InMemoryRepository implements Repository
{
    /**
     * @var Question[]
     */
    private $questions;

    /**
     * InMemoryRepository constructor.
     * @param Question[] $questions
     */
    public function __construct(array $questions = [])
    {
        $this->questions = $questions;
    }

    /**
     * @param Id $id
     * @return Question
     */
    public function get(Id $id): Question
    {
        if (!isset($this->questions[$id->getValue()])) {
            throw new \InvalidArgumentException('Cannot find question with the ID');
        }

        return $this->questions[$id->getValue()];
    }

    /**
     * @param string $content
     * @return Question
     */
    public function add(string $content): Question
    {
        $question = new Question(new Id(rand(0,1000)), new Question\Content($content), new Question\CreatedAt(new \DateTimeImmutable()));
        $this->questions[$question->getId()->getValue()] = $question;

        return $question;
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->questions;
    }

    /**
     * @param int $questionId
     * @param string $content
     * @return Question
     */
    public function updateQuestion(int $questionId, string $content): Question
    {
        if (!isset($this->questions[$questionId])) {
            throw new \InvalidArgumentException(sprintf('The question with id %d does not exist', $questionId));
        }

        $this->questions[$questionId]->updateContent($content);

        return $this->questions[$questionId];
    }

    /**
     * @param int $questionId
     */
    public function delete(int $questionId): void
    {
        if (!isset($this->questions[$questionId])) {
            throw new \InvalidArgumentException(sprintf('The question with id %d does not exist', $questionId));
        }

        unset($this->questions[$questionId]);
    }
}
