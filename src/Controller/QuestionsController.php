<?php

namespace Brainly\Controller;

use Brainly\Application\QuestionService;
use Gnugat\Marshaller\Marshaller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class QuestionsController
{
    /**
     * @var QuestionService
     */
    private $questionsService;

    /**
     * @var Marshaller
     */
    private $marshaller;

    /**
     * QuestionsController constructor.
     * @param QuestionService $questionsService
     * @param Marshaller $marshaller
     */
    public function __construct(QuestionService $questionsService, Marshaller $marshaller)
    {
        $this->questionsService = $questionsService;
        $this->marshaller = $marshaller;
    }

    public function listAction()
    {
        $questions = $this->questionsService->getAll();

        return new JsonResponse($this->marshaller->marshalCollection($questions));
    }

    public function addAction(Request $request)
    {
        $requestData = json_decode($request->getContent());

        try {
            $question = $this->questionsService->addQuestion($requestData->content);

            return new JsonResponse([
                'question' => $this->marshaller->marshal($question),
            ]);
        } catch (\InvalidArgumentException $exception) {
            return new JsonResponse(array(
                'message' => $exception->getMessage(),
            ), 400);
        }
    }

    public function deleteAction(int $questionId)
    {
        $this->questionsService->delete($questionId);

        return new Response('', 204);
    }

    public function getAction(int $questionId)
    {
        try {
            $question = $this->questionsService->get($questionId);

            return new JsonResponse([
                'question' => $this->marshaller->marshal($question),
            ]);
        } catch (\InvalidArgumentException $exception) {
            return new JsonResponse(array(
                'message' => $exception->getMessage(),
            ), 404);
        }
    }

    public function updateAction(int $questionId, Request $request)
    {
        $requestData = json_decode($request->getContent());

        try {
            $question = $this->questionsService->updateQuestion($questionId, $requestData->content);

            return new JsonResponse([
                'question' => $this->marshaller->marshal($question),
            ]);
        } catch (\InvalidArgumentException $exception) {
            return new JsonResponse(array(
                'message' => $exception->getMessage(),
            ), 400);
        }
    }
}
