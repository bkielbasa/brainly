<?php

namespace Brainly\Controller;

use Brainly\Application\QuestionService;
use Gnugat\Marshaller\Marshaller;
use Gnugat\Marshaller\NotSupportedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AnswerController
{
    /**
     * @var QuestionService
     */
    private $questionsService;

    /**
     * @var Marshaller
     */
    private $marshaller;

    /**
     * AnswerController constructor.
     * @param QuestionService $questionsService
     * @param Marshaller $marshaller
     */
    public function __construct(QuestionService $questionsService, Marshaller $marshaller)
    {
        $this->questionsService = $questionsService;
        $this->marshaller = $marshaller;
    }

    public function addAction(int $questionId, Request $request)
    {
        $requestData = json_decode($request->getContent());

        try {
            $answer = $this->questionsService->addAnswer($questionId, $requestData->content);

            return new JsonResponse([
                'answer' => $this->marshaller->marshal($answer),
            ]);
        } catch (\InvalidArgumentException $exception) {
            return new JsonResponse(array(
                'message' => $exception->getMessage(),
            ), 400);
        }
    }

    public function deleteAction(int $answerId, int $questionId)
    {
        try {
            $this->questionsService->deleteAnswer($questionId, $answerId);

            return new Response('', 204);
        } catch (\InvalidArgumentException $exception) {
            return new JsonResponse(array(
                'message' => $exception->getMessage(),
            ), 404);
        }
    }

    public function getAction(int $questionId, $answerId)
    {
        try {
            $answer = $this->questionsService->getAnswer($questionId, $answerId);

            return new JsonResponse([
                'answer' => $this->marshaller->marshal($answer),
            ]);
        } catch (\InvalidArgumentException $exception) {
            return new JsonResponse(array(
                'message' => $exception->getMessage(),
            ), 404);
        }
    }

    public function listAction(int $questionId)
    {
        $answers = $this->questionsService->getAnswers($questionId);

        return new JsonResponse($this->marshaller->marshalCollection($answers));
    }

    public function updateAction(int $questionId, int $answerId, Request $request)
    {
        $requestData = json_decode($request->getContent());

        try {
            $answer = $this->questionsService->updateAnswer($questionId, $answerId, $requestData->content);

            return new JsonResponse([
                'answer' => $this->marshaller->marshal($answer),
            ]);
        } catch (\InvalidArgumentException $exception) {
            return new JsonResponse(array(
                'message' => $exception->getMessage(),
            ), 404);
        }
    }
}