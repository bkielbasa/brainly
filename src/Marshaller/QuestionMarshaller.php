<?php

namespace Brainly\Marshaller;

use Brainly\Application\Question\QuestionDTO;
use Gnugat\Marshaller\MarshallerStrategy;

class QuestionMarshaller implements MarshallerStrategy
{

    /**
     * @param mixed $toMarshal
     * @param string $category
     *
     * @return bool
     *
     * @api
     */
    public function supports($toMarshal, $category = null)
    {
        return $toMarshal instanceof QuestionDTO;
    }

    /**
     * @param mixed $toMarshal
     *
     * @return mixed
     *
     * @api
     */
    public function marshal($toMarshal)
    {
        return [
            'id' => $toMarshal->getId(),
            'content' => $toMarshal->getContent(),
            'createdAt' => $toMarshal->getCreatedAt(),
            'answers' => $toMarshal->getAnswers(),
        ];
    }
}