<?php

namespace Brainly\Marshaller;

use Brainly\Application\Answer\AnswerDTO;
use Gnugat\Marshaller\MarshallerStrategy;

class AnswerMarshaller implements MarshallerStrategy
{

    /**
     * @param mixed $toMarshal
     * @param string $category
     *
     * @return bool
     *
     * @api
     */
    public function supports($toMarshal, $category = null)
    {
        return $toMarshal instanceof AnswerDTO;
    }

    /**
     * @param mixed $toMarshal
     *
     * @return array
     *
     * @api
     */
    public function marshal($toMarshal)
    {
        return [
            'id' => $toMarshal->getId(),
            'content' => $toMarshal->getContent(),
            'createdAt' => $toMarshal->getCreatedAt(),
        ];
    }
}