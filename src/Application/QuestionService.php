<?php

namespace Brainly\Application;

use Brainly\Application\Answer\AnswerDTO;
use Brainly\Application\Question\QuestionDTO;
use Brainly\Domain\Question;
use Brainly\Domain\Answer;
use Brainly\Domain\Question\Id;

class QuestionService
{
    /**
     * @var Question\Repository
     */
    private $questionRepository;

    /**
     * @var Answer\Repository
     */
    private $answerRepository;

    /**
     * @var int
     */
    private $maximumAnswers;

    /**
     * @var int
     */
    private $minimumContentLen;

    /**
     * @var int
     */
    private $maximumContentLen;

    /**
     * QuestionService constructor.
     * @param Question\Repository $questionRepository
     * @param Answer\Repository $answerRepository
     * @param int $maximumAnswers
     * @param int $minimumContentLen
     * @param int $maximumContentLen
     */
    public function __construct(Question\Repository $questionRepository, Answer\Repository $answerRepository, int $maximumAnswers, int $minimumContentLen, int $maximumContentLen)
    {
        $this->questionRepository = $questionRepository;
        $this->answerRepository = $answerRepository;
        $this->maximumAnswers = $maximumAnswers;
        $this->minimumContentLen = $minimumContentLen;
        $this->maximumContentLen = $maximumContentLen;
    }

    /**
     * @param int $questionId
     * @param string $content
     * @return AnswerDTO
     */
    public function addAnswer(int $questionId, string $content): AnswerDTO
    {
        $question = $this->questionRepository->get(new Question\Id($questionId));
        $answers = $this->answerRepository->getAnswers($question->getId()->getValue());

        if (count($answers) == $this->maximumAnswers) {
            throw new \InvalidArgumentException(sprintf('The question already has %s answers', $this->maximumAnswers));
        }

        if (strlen($content) < $this->minimumContentLen) {
            throw new \InvalidArgumentException(sprintf('The content cannot be shorter than %d', $this->minimumContentLen));
        }

        if (strlen($content) > $this->maximumContentLen) {
            throw new \InvalidArgumentException(sprintf('The content cannot be longer than %d', $this->maximumContentLen));
        }

        $answer = $this->answerRepository->add($question->getId()->getValue(), $content);

        return new AnswerDTO($answer);
    }

    /**
     * @param $content
     * @return QuestionDTO
     */
    public function addQuestion($content): QuestionDTO
    {
        if (strlen($content) < $this->minimumContentLen) {
            throw new \InvalidArgumentException(sprintf('The content cannot be shorter than %d', $this->minimumContentLen));
        }

        if (strlen($content) > $this->maximumContentLen) {
            throw new \InvalidArgumentException(sprintf('The content cannot be longer than %d', $this->maximumContentLen));
        }

        return new QuestionDTO($this->questionRepository->add($content));
    }

    /**
     * @return QuestionDTO[]
     */
    public function getAll(): array
    {
        $questions = $this->questionRepository->getAll();
        $questionDTOs = [];

        foreach ($questions as $question) {
            $questionDTOs[] = new QuestionDTO($question, $this->answerRepository->getAnswers($question->getId()->getValue()));
        }

        return $questionDTOs;
    }

    public function updateQuestion($questionId, $content):QuestionDTO
    {
        $question = $this->questionRepository->updateQuestion($questionId, $content);

        return new QuestionDTO($question, $this->answerRepository->getAnswers($questionId));
    }

    /**
     * @param int $questionId
     */
    public function delete(int $questionId): void
    {
        $this->questionRepository->delete($questionId);
    }

    public function get(int $questionId): QuestionDTO
    {
        $question = $this->questionRepository->get(new Id($questionId));

        return new QuestionDTO($question, $this->answerRepository->getAnswers($question->getId()->getValue()));
    }

    /**
     * @param int $questionId
     * @return AnswerDTO[]
     */
    public function getAnswers(int $questionId): array
    {
        $answers = $this->answerRepository->getAnswers($questionId);
        $answerDTOs = [];

        foreach ($answers as $answer) {
            $answerDTOs[] = new AnswerDTO($answer);
        }

        return $answerDTOs;
    }

    /**
     * @param int $questionId
     * @param int $answerId
     * @return AnswerDTO
     */
    public function getAnswer(int $questionId, $answerId): AnswerDTO
    {
        return new AnswerDTO($this->answerRepository->get(new Answer\Id($answerId)));
    }

    /**
     * @param int $questionId
     * @param int $answerId
     */
    public function deleteAnswer(int $questionId, int $answerId): void
    {
        $this->answerRepository->remove(new Answer\Id($answerId));
    }

    /**
     * @param int $questionId
     * @param int $answerId
     * @param string $content
     * @return AnswerDTO
     */
    public function updateAnswer(int $questionId, int $answerId, string $content): AnswerDTO
    {
        return new AnswerDTO($this->answerRepository->update(new Answer\Id($answerId), new Answer\Content($content)));
    }
}
