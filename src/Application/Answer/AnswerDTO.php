<?php

namespace Brainly\Application\Answer;

use Brainly\Domain\Answer;

class AnswerDTO
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $content;

    /**
     * @var int
     */
    private $createdAt;

    public function __construct(Answer $answer)
    {
        $this->id = $answer->getId()->getValue();
        $this->content = $answer->getContent()->getValue();
        $this->createdAt = $answer->getCreatedAt()->getValue()->getTimestamp();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return int
     */
    public function getCreatedAt(): int
    {
        return $this->createdAt;
    }
}
