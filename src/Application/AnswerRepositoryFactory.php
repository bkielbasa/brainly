<?php

namespace Brainly\Application;

use Brainly\Domain\Answer;
use Brainly\Domain\Answer\Content;
use Brainly\Domain\Answer\Id;
use Brainly\Infrastructure\Answer\Repository\InMemoryRepository;

class AnswerRepositoryFactory
{
    public static function create(): Answer\Repository
    {
        return new InMemoryRepository([
            1 => new Answer(new Id(1), new Content('1234567890 1234567890 1234567890'), 1),
            2 => new Answer(new Id(2), new Content('Some other answer goes here '), 1),
            3 => new Answer(new Id(3), new Content('And other answer goes here '), 1),
        ]);
    }
}
