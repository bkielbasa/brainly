<?php

namespace Brainly\Application;

use Brainly\Domain\Question;
use Brainly\Domain\Question\Content;
use Brainly\Domain\Question\Id;
use Brainly\Infrastructure\Question\Repository\InMemoryRepository;

class QuestionRepositoryFactory
{
    public static function create(): Question\Repository
    {
        return new InMemoryRepository([
            1 => new Question(new Id(1), new Content('1234567890 1234567890 1234567890 '))
        ]);
    }
}
