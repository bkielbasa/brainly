<?php

namespace Brainly\Application\Question;

use Brainly\Domain\Answer;
use Brainly\Domain\Question;

class QuestionDTO
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $content;

    /**
     * @var int
     */
    private $createdAt;

    /**
     * @var array
     */
    private $answers = [];

    /**
     * QuestionDTO constructor.
     * @param Question $question
     * @param Answer[] $answers
     */
    public function __construct(Question $question, array $answers = [])
    {
        $this->id = $question->getId()->getValue();
        $this->content = $question->getContent()->getValue();
        $this->createdAt = $question->getCreatedAt()->getValue()->getTimestamp();

        foreach ($answers as $answer) {
            $this->answers[] = [
                'id' => $answer->getId()->getValue(),
                'content' => $answer->getContent()->getValue(),
                'createdAt' => $answer->getCreatedAt()->getValue()->getTimestamp(),
            ];
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return int
     */
    public function getCreatedAt(): int
    {
        return $this->createdAt;
    }

    /**
     * @return array
     */
    public function getAnswers(): array
    {
        return $this->answers;
    }
}
