<?php

namespace Brainly\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="question_answer")
 */
class Answer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $content;

    /**
     * @ORM\ManyToOne(targetEntity="Brainly\Entity\Question", inversedBy="answers")
     * @ORM\JoinColumn(nullable=true)
     */
    private $question;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $createdAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return Question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @return int
     */
    public function getCreatedAt(): int
    {
        return $this->createdAt;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @param int $createdAT
     */
    public function setCreatedAt(int $createdAT): void
    {
        $this->createdAt = $createdAT;
    }

    public function setQuestion($questionEntity)
    {
        $this->question = $questionEntity;
    }
}