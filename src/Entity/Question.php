<?php

namespace Brainly\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="question")
 */
class Question
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $content;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $createdAT;

    /**
     * @ORM\OneToMany(targetEntity="Brainly\Entity\Answer", mappedBy="question")
     */
    private $answers;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return int
     */
    public function getCreatedAt(): int
    {
        return $this->createdAT;
    }

    /**
     * @return Answer[]
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @param int $createdAT
     */
    public function setCreatedAT(int $createdAT): void
    {
        $this->createdAT = $createdAT;
    }
}
