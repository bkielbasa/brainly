<?php

namespace Brainly\Test\Application;

use Brainly\Application\QuestionService;
use Brainly\Domain\Question\Id;
use Brainly\Infrastructure\Question;
use Brainly\Infrastructure\Answer;
use PHPUnit\Framework\TestCase;

class QuestionServiceTest extends TestCase
{
    public function testAddNewQuestion()
    {
        // given
        $questionRepository = new Question\Repository\InMemoryRepository();
        $answerRepository = new Answer\Repository\InMemoryRepository();
        $service = new QuestionService($questionRepository, $answerRepository, 2, 20, 5000);
        $content = str_repeat('some sample question', 20);

        //when
        $question = $service->addQuestion($content);

        //then
        $this->assertGreaterThan(0, $question->getId());
    }

    public function testAddAnswerToExistingQuestion()
    {
        // given
        $questionId = 1;
        $questionRepository = new Question\Repository\InMemoryRepository([
            $questionId => new \Brainly\Domain\Question(new Id($questionId), new \Brainly\Domain\Question\Content(str_repeat( 'a', 51))),
        ]);
        $answerRepository = new Answer\Repository\InMemoryRepository();
        $service = new QuestionService($questionRepository, $answerRepository, 2, 20, 5000);
        $content = str_repeat('some answer', 20);

        //when
        $answer = $service->addAnswer($questionId, $content);

        //then
        $this->assertGreaterThan(0, $answer->getId());
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testAddTooMuchAnswers()
    {
        // given
        $questionId = 1;
        $questionRepository = new Question\Repository\InMemoryRepository([
            $questionId => new \Brainly\Domain\Question(new Id($questionId), new \Brainly\Domain\Question\Content(str_repeat( 'a', 51))),
        ]);
        $answerRepository = new Answer\Repository\InMemoryRepository();
        $service = new QuestionService($questionRepository, $answerRepository, 2, 20, 5000);
        $content = 'some answer';

        //when and then
        $service->addAnswer($questionId, $content);
        $service->addAnswer($questionId, $content);
        $service->addAnswer($questionId, $content);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testAddAnswerToNonExistingQuestion()
    {
        // given
        $questionId = 666;
        $questionRepository = new Question\Repository\InMemoryRepository([
            1 => new \Brainly\Domain\Question(new Id(1), new \Brainly\Domain\Question\Content(str_repeat( 'a', 51))),
        ]);
        $answerRepository = new Answer\Repository\InMemoryRepository();
        $service = new QuestionService($questionRepository, $answerRepository, 2, 20, 5000);
        $content = 'some answer';

        //when and then
        $service->addAnswer($questionId, $content);
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage The content cannot be shorter than 20
     */
    public function testQuestionWithTooShortContent()
    {
        // given
        $questionRepository = new Question\Repository\InMemoryRepository();
        $answerRepository = new Answer\Repository\InMemoryRepository();
        $service = new QuestionService($questionRepository, $answerRepository, 2, 20, 5000);
        $content = 'too short';

        //when and then
        $service->addQuestion($content);
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage The content cannot be longer than 10
     */
    public function testQuestionWithTooLongContent()
    {
        // given
        $questionRepository = new Question\Repository\InMemoryRepository();
        $answerRepository = new Answer\Repository\InMemoryRepository();
        $service = new QuestionService($questionRepository, $answerRepository, 2, 5, 10);
        $content = 'too long content is here because it\'s more thant 10';

        //when and then
        $service->addQuestion($content);
    }
}
