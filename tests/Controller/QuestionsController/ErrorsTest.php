<?php

namespace App\Tests\Controller\QuestionsController;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ErrorsTest extends WebTestCase
{
    public function testFetchingNotExistingQuestion()
    {
        // given
        $client = static::createClient();
        $invalidId = 0;

        // when
        $client->request('GET', '/v1/questions/' . $invalidId);
        $response = $client->getResponse();
        $jsonResponse = json_decode($response->getContent(), true);

        // then
        $this->assertNotNull($jsonResponse, 'Response should be empty');
        $this->assertSame(404, $response->getStatusCode());
    }
}
