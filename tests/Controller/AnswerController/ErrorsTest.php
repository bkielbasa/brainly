<?php

namespace App\Tests\Controller\AnswerController;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ErrorsTest extends WebTestCase
{
    public function testFetchingNotExistingAnswer()
    {
        // given
        $client = static::createClient();
        $invalidAnswerId = 0;

        // when
        $client->request('GET', '/v1/question/1/answers/' . $invalidAnswerId);
        $response = $client->getResponse();
        json_decode($response->getContent(), true);

        // then
        $this->assertSame(404, $response->getStatusCode());
    }

    public function testDeleteNotExistingAnswer()
    {
        // given
        $client = static::createClient();
        $invalidAnswerId = 0;

        // when
        $client->request('DELETE', '/v1/question/1/answers/' . $invalidAnswerId);
        $response = $client->getResponse();
        json_decode($response->getContent(), true);

        // then
        $this->assertSame(404, $response->getStatusCode());
    }

    public function testEditNotExistingAnswer()
    {
        // given
        $client = static::createClient();
        $questionContent = 'Lorem ipsum, Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum';
        $invalidAnswerId = 0;

        // when
        $client->request('PUT', '/v1/question/1/answers/' . $invalidAnswerId, [], [], [], json_encode([
            'content' => $questionContent,
        ]));
        $response = $client->getResponse();
        $jsonResponse = json_decode($response->getContent(), true);

        // then
        $this->assertNotNull($jsonResponse, 'Response should be a valid JSON');
        $this->assertSame(404, $response->getStatusCode());
    }
}
