<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AnswersControllerTest extends WebTestCase
{
    public function testList()
    {
        // given
        $client = static::createClient();

        // when
        $client->request('GET', '/v1/question/1/answers');
        $response = $client->getResponse();

        // then
        $this->assertSame(200, $response->getStatusCode());
    }

    public function testGet()
    {
        // given
        $client = static::createClient();
        $expectedContent = '1234567890 1234567890 1234567890';

        // when
        $client->request('GET', '/v1/question/1/answers/1');
        $response = $client->getResponse();
        $jsonResponse = json_decode($response->getContent(), true);

        // then
        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame($expectedContent, $jsonResponse['answer']['content']);
    }

    public function testAdd()
    {
        // given
        $client = static::createClient();
        $questionContent = 'Lorem ipsum, Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum';

        // when
        $client->request('POST', '/v1/question/1/answers', [], [], [], json_encode([
            'content' => $questionContent,
        ]));
        $response = $client->getResponse();
        $jsonResponse = json_decode($response->getContent(), true);

        // then
        $this->assertNotNull($jsonResponse, 'Response should be a valid JSON');
        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame($questionContent, $jsonResponse['answer']['content']);
        $this->assertGreaterThan(0, (int)$jsonResponse['answer']['id']);
    }

    public function testEdit()
    {
        // given
        $client = static::createClient();
        $questionContent = 'Lorem ipsum, Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum';

        // when
        $client->request('PUT', '/v1/question/1/answers/1', [], [], [], json_encode([
            'content' => $questionContent,
        ]));
        $response = $client->getResponse();
        $jsonResponse = json_decode($response->getContent(), true);

        // then
        $this->assertNotNull($jsonResponse, 'Response should be a valid JSON');
        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame($questionContent, $jsonResponse['answer']['content']);
        $this->assertGreaterThan(0, (int)$jsonResponse['answer']['id']);
    }

    public function testDelete()
    {
        // given
        $client = static::createClient();

        // when
        $client->request('DELETE', '/v1/question/1/answers/1');
        $response = $client->getResponse();

        // then
        $this->assertEmpty($response->getContent(), 'Response should be empty');
        $this->assertSame(204, $response->getStatusCode());
    }
}
