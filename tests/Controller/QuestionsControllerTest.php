<?php

namespace Brainly\Test\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class QuestionsControllerTest extends WebTestCase
{
    public function testListQuestions()
    {
        // given
        $client = static::createClient();

        // when
        $client->request('GET', '/v1/questions');
        $response = $client->getResponse();

        // then
        $this->assertSame(200, $response->getStatusCode());
    }

    public function testAddQuestion()
    {
        // given
        $client = static::createClient();
        $questionContent = 'Lorem ipsum, Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum';

        // when
        $client->request('POST', '/v1/questions', [], [], [], json_encode([
            'content' => $questionContent,
        ]));
        $response = $client->getResponse();
        $jsonResponse = json_decode($response->getContent(), true);

        // then
        $this->assertNotNull($jsonResponse, 'Response should be a valid JSON');
        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame($questionContent, $jsonResponse['question']['content']);
        $this->assertNotNull($jsonResponse['question']['answers'], 'The question should contain answers');
        $this->assertGreaterThan(0, (int)$jsonResponse['question']['id']);
    }

    public function testEditQuestionsContent()
    {
        // given
        $client = static::createClient();
        $questionContent = 'New content 1234567890 1234567890';

        // when
        $client->request('PUT', '/v1/questions/1', [], [], [], json_encode([
            'content' => $questionContent,
        ]));
        $response = $client->getResponse();
        $jsonResponse = json_decode($response->getContent(), true);

        // then
        $this->assertNotNull($jsonResponse, 'Response should be a valid JSON');
        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame($questionContent, $jsonResponse['question']['content']);
    }

    public function testDeleteQuestionsContent()
    {
        // given
        $client = static::createClient();

        // when
        $client->request('DELETE', '/v1/questions/1');
        $response = $client->getResponse();

        // then
        $this->assertEmpty($response->getContent(), 'Response should be empty');
        $this->assertSame(204, $response->getStatusCode());
    }

    public function testGetQuestionsContent()
    {
        // given
        $client = static::createClient();

        // when
        $client->request('GET', '/v1/questions/1');
        $response = $client->getResponse();
        $jsonResponse = json_decode($response->getContent(), true);

        // then
        $this->assertNotNull($jsonResponse, 'Response should be empty');
        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame(1, $jsonResponse['question']['id']);
    }
}
